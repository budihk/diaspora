#!/bin/bash --login

cd diaspora
mkdir -p tmp/pids
mkdir -p tmp/cache

export RAILS_ENV=production 

/home/diaspora/startup-lighttpd.sh &
bin/rake db:migrate
script/server
