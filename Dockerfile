FROM debian:buster-slim as build

COPY run_as_diaspora.sh /run_as_diaspora.sh

# hack to make postgresql-client install work on slim
RUN mkdir -p /usr/share/man/man1 \
    && mkdir -p /usr/share/man/man7

RUN apt-get update && \
	apt-get install -y -qq \
	cmake \
	postgresql-client \
	build-essential \
	libgmp-dev \
	libssl-dev \
	libcurl4-openssl-dev \
	libxml2-dev \
	libxslt-dev \
	imagemagick \
	ghostscript \
	git \
	curl \
	libpq-dev \
        libidn11-dev \
	libmagickwand-dev \
	nodejs \
	gawk \
	libreadline6-dev \
	libyaml-dev \
	libsqlite3-dev \
	sqlite3 \
	autoconf \
	libgdbm-dev \
	libncurses5-dev \
	automake \
	bison \
	libffi-dev 

RUN adduser --gecos "" --home /home/diaspora diaspora
COPY compose/diaspora.yml.example /diaspora.yml
COPY diaspora.toml /diaspora.toml

ARG GIT_URL=https://github.com/diaspora/diaspora.git
ARG GIT_BRANCH=master
ARG RUBY_VERSION=2.7
ARG GEM_VERSION=3.4.5
ARG DEBIAN_FRONTEND=noninteractive

RUN su diaspora -c '/run_as_diaspora.sh'

# reduce image size by deleting files unnecessary at runtime
RUN rm -rf /home/diaspora/diaspora/.git \ 
           /home/diaspora/.gnupg \
           /home/diaspora/diaspora/vendor/bundle/ruby/**/cache && \
    find /home/diaspora/diaspora -name spec -exec rm -rf {} \+ 
	
COPY startup.sh startup-lighttpd.sh /home/diaspora/

FROM debian:buster-slim

ARG DIASPORA_DOCKER_GIT_COMMIT=unspecified

LABEL maintainer="Brad Koehn <brad@koe.hn>"
LABEL diaspora_docker_git_commit=$DIASPORA_DOCKER_GIT_COMMIT

RUN adduser --gecos "" --disabled-login --home /home/diaspora diaspora 
COPY --chown=diaspora:diaspora --from=build /home/diaspora /home/diaspora

RUN apt-get update && \
    apt-get install -yqq \
        postgresql-client \
        imagemagick \
        libyaml-0-2 \
        libgmp10 \
        libjemalloc2 \
        libssl1.1 \
        libxml2 \ 
        libxslt1.1 \
        libpq5 \ 
        libmagickwand-6.q16-6 \
        libreadline7 \
        libsqlite3-0 \ 
        libgdbm6 \ 
        libncurses5 \
        ghostscript \
        curl \
        ca-certificates \
        nodejs \
        gawk \
        procps \
        sqlite3 \
        lighttpd && \
    rm -rf /var/lib/apt/lists /tmp/* /var/tmp/* && \
    ln -s /home/diaspora/diaspora/public/assets /var/www/html/assets && \
    chown -R diaspora:diaspora /var/cache/lighttpd /var/log/lighttpd

COPY --chown=www-data:www-data lighttpd.conf /etc/lighttpd/lighttpd.conf

USER diaspora

WORKDIR /home/diaspora

CMD ["/home/diaspora/startup.sh"]

